var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    runSequence = require('run-sequence')
    clean = require('gulp-clean'),
    autoPrefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    flatmap = require('gulp-flatmap'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    babel = require('rollup-plugin-babel'),
    rollup = require('gulp-better-rollup'),
    rollupNodeResolve = require('rollup-plugin-node-resolve'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin');

// Clean dist/prod
gulp.task('prod-clean', function() {
  return gulp.src('./dist/prod', { read: false })
    .pipe(clean({ force: true }));
});

// Copy php files
gulp.task('prod-php', function() {
  return gulp.src('./src/**/*.php')
    .pipe(gulp.dest('./dist/prod'));
});

// Build and autoprefix styles
gulp.task('prod-styles', function() {
  return gulp.src(['./src/**/*.css', '!./src/assets/**/*.css'])
    .pipe(flatmap(function(stream, file) {
      var outPath = path.join('./dist/prod', path.dirname(file.relative));
      return gulp.src(['./src/assets/css/*.css', file.path])
        .pipe(sourcemaps.init())
          .pipe(cleanCSS({
            rebase: false,
            normalizeUrls: false
          }))
          .pipe(autoPrefixer())
          .pipe(concat('styles.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outPath));
    }));
});

// Build javascript with browserify
gulp.task('prod-js', function() {
  return gulp.src(['./src/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(sourcemaps.init())
    .pipe(rollup({
      plugins: [
        babel(),
        rollupNodeResolve()
      ],
      onwarn: function(warning) {
        // Skip certain warnings
        if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
        if ((warning) && (warning.message)) {
          console.warn( warning.message );
        }
      }
    }, 'cjs'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/prod'));
});

// Minify images
gulp.task('prod-images', function() {
  return gulp.src('./src/assets/images/**/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/prod/assets/images'));
});

// Prod server
gulp.task('prod', function() {
  var tasks = ['prod-clean', 'prod-php', 'prod-js', 'prod-styles', 'prod-images'];

  // Initial build
  runSequence(...tasks, function() {
    // Proxy the box
    browserSync({
      proxy: 'http://192.168.33.10/prod/'
    });
  });

  // Rebuild on changes
  gulp.watch('./src/**/*.*').on('change', function () {
    runSequence(...tasks, function() {
      // Refresh browser
      browserSync.reload();
    });
  });
});

// Prod build
gulp.task('prod-build', function() {
  var tasks = ['prod-clean', 'prod-php', 'prod-js', 'prod-styles', 'prod-images'];
  runSequence(...tasks);
});