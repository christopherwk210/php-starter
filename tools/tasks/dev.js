var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    runSequence = require('run-sequence')
    clean = require('gulp-clean'),
    autoPrefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    flatmap = require('gulp-flatmap'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    babel = require('rollup-plugin-babel'),
    rollup = require('gulp-better-rollup'),
    rollupNodeResolve = require('rollup-plugin-node-resolve');

// Clean dist/dev
gulp.task('dev-clean', function() {
  return gulp.src('./dist/dev', { read: false })
    .pipe(clean({ force: true }));
});

// Copy php files
gulp.task('dev-php', function() {
  return gulp.src('./src/**/*.php')
    .pipe(gulp.dest('./dist/dev'));
});

// Build and autoprefix styles
gulp.task('dev-styles', function() {
  return gulp.src(['./src/**/*.css', '!./src/assets/**/*.css'])
    .pipe(flatmap(function(stream, file) {
      var outPath = path.join('./dist/dev', path.dirname(file.relative));
      return gulp.src(['./src/assets/css/*.css', file.path])
        .pipe(sourcemaps.init())
          .pipe(autoPrefixer())
          .pipe(concat('styles.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outPath));
    }));
});

// Build javascript with browserify
gulp.task('dev-js', function() {
  return gulp.src(['./src/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(sourcemaps.init())
    .pipe(rollup({
      plugins: [
        babel(),
        rollupNodeResolve()
      ],
      onwarn: function(warning) {
        // Skip certain warnings
        if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
        if ((warning) && (warning.message)) {
          console.warn( warning.message );
        }
      }
    }, 'cjs'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/dev'));
});

// Copy images
gulp.task('dev-images', function() {
  return gulp.src('./src/assets/images/**/*.*')
    .pipe(gulp.dest('dist/dev/assets/images'));
});

// Dev server
gulp.task('dev', function() {
  var tasks = ['dev-clean', 'dev-php', 'dev-js', 'dev-styles', 'dev-images'];

  // Initial build
  runSequence(...tasks, function() {
    // Proxy the box
    browserSync({
      proxy: 'http://192.168.33.10/dev/'
    });
  });

  // Rebuild on changes
  gulp.watch('./src/**/*.*').on('change', function () {
    runSequence(...tasks, function() {
      // Refresh browser
      browserSync.reload();
    });
  });
});

// Dev build
gulp.task('dev-build', function() {
  var tasks = ['dev-clean', 'dev-php', 'dev-js', 'dev-styles', 'dev-images'];
  runSequence(...tasks);
});