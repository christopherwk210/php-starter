/*
  Simple generator script to create new pages quickly.
*/
var fs = require('fs');
var rootDir = './src';

/**
 * Simple function to prompt the user at the command line
 * @param {string} question Prompt
 * @param {string} callback Callback function
 */
function prompt(question, callback) {
    var stdin = process.stdin,
        stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function (data) {
        callback(data.toString().trim());
    });
}

// :P
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// Prompt the user for the page name they want
prompt('Name of new page to add (enter to cancel): ', function(res) {
  if (res.length === 0) {
    process.exit();
  }

  if (res.indexOf(' ') >= 0) {
    console.error('Page name can\'t have whitespaces!');
    process.exit(1);
  }

  // Fancyfy
  var lower = res.toLowerCase();
  var initCameled = res.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
  var cameled = capitalizeFirstLetter(initCameled);

  console.log('Adding new page \'' + res + '\'...');

  var newDir = rootDir + '/' + lower;

  // Make the folder
  if (!fs.existsSync(newDir)) {
    fs.mkdirSync(newDir);
  } else {
    console.error('Page exists already...');
    process.exit(1);
  }

  // Create a JS file
  try {
    fs.writeFileSync(newDir + '/' + lower + '.js', '', 'utf8');
  } catch(e) {
    console.log(e);
    process.exit(1);
  }
  console.log('Created > ' + newDir + '/' + lower + '.js');

  // Create a CSS file
  try {
    fs.writeFileSync(newDir + '/' + lower + '.css', '', 'utf8');
  } catch(e) {
    console.log(e);
    process.exit(1);
  }
  console.log('Created > ' + newDir + '/' + lower + '.css');

  // Create a php file
  var indexText = `<?php include '../assets/components/config.php'; ?>

<!DOCTYPE html>
<html lang="en">
  <?php
    $site_title .= ' | ${cameled}';
    include '../assets/components/head-tag.php';
  ?>
  <body>
    <h1>${cameled} page!</h1>

    <script src="${lower}.js"></script>
  </body>
</html>
`;
  try {
    fs.writeFileSync(newDir + '/index.php', indexText, 'utf8');
  } catch(e) {
    console.log(e);
    process.exit(1);
  }
  console.log('Created > ' + newDir + '/index.php');

  // Done!
  console.log('Done!');
  console.log('--------');

  process.exit();
})
