# PHP Starter
A very simple build environment for quickly creating websites. This framework was created with simple personal or local business websites in mind. It is not intended to be used for complex projects, and is completely focussed around the front end.

Babel, Rollup, and jQuery are all included by default to make life easy in JavaScript land. The inluded Gulp build setup also includes jshint, css-autoprefixer, and minification with sourcemaps for production builds.

## Quick Start
Make sure you have Vagrant installed on your machine, then  `npm i && npm start` and your good to go.

To build for production, use `npm run build.prod`. If you want to test production capabilities, you can run the browsersync server on it with `npm run prod`.

## Generate Pages
A simple CLI command is included to create pages with `npm run g`. This will create a new folder for your page, and supply it with it's own stylesheet and javascript file properly linked in a generated php file.