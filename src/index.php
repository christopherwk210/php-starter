<?php include 'assets/components/config.php'; ?>

<!DOCTYPE html>
<html lang="en">
  <?php
    $site_title .= ' | Home';
    include 'assets/components/head-tag.php';
  ?>
  <body>
    <h1>Hello, world!</h1>

    <script src="home.js"></script>
  </body>
</html>