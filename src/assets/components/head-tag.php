<head>
  <!--<base target="_blank">-->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<meta content="width=device-width,initial-scale=1" name="viewport">
  <meta content="<?php echo $site_description; ?>" name="description" />
  <meta content="<?php echo $site_title; ?>" property="og:title" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo $site_url; ?>" />
  <meta property="og:image" content="https://mycoolsite.com/path/to/image.png" />
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $site_title; ?>" />
  <meta name="twitter:description" content="<?php echo $site_description; ?>" />
	<title><?php echo $site_title; ?></title>
	<link href="styles.css" rel="stylesheet">
</head>